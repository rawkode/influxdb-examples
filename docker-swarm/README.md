# Docker Swarm Monitoring

This compose file will deploy InfluxDB 2.

Telegraf should be run on the host of each machine, to consume the Docker Engine metrics.

You can apply the notifications, aggregation tasks, and dashboards with:

```console
influx -t token pkg -o org_name apply -f pkger.yaml
```
